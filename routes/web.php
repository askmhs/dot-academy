<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template',['kategori' => []]);
});

Route::get('/nama', function (){
	return "Gandhy";
});

Route::get('/helo/{nama}/{sapa}', function($jeneng,$sapa){
	return "Hello $jeneng $sapa";
});

Route::get('/sapa', 'CobaController@sapa');
Route::get('/sapa/{nama}', 'CobaController@sapaNama');

Route::post('/kategori', 'KategoriController@submit');
